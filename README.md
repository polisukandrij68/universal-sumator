# Universal Sumator

## This is universal js sumator. Created for KNUTE Computer Archicture labaratory work

**About Project**

* src/Engine.ts - main calculation file
* src/index.ts - file with tests
* src/Number.ts - universal class for number casting
* src/BinaryNumber.ts - extends from Number and has additional methods for working with binary numbers
* src/DecimalNumber.ts - extends from Number and has additional methods for working with decimal numbers

---

**Main Commands**

*start file with tests*
```
npm run start
```
*build a project*
```
npm run build
```
*will start process witch will rebuild project when changes in files occurs and after that it will execute program*
```
npm run dev
```

**Created by Andrii Polishchuk**