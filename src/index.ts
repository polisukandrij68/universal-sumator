import { Engine } from './Engine';
import { Number } from './Number';
import { BinaryNumber } from './BinaryNumber';
import { DecimalNumber } from './DecimalNumber'; 

export default {
  Engine,
  Number,
  BinaryNumber,
  DecimalNumber
} 