export class Number {
    constructor(
      public readonly value: string | number,
      public readonly kind: number
    ) {}
  
    log(){
        console.log('My value is - '+this.value);
        return this;
    }
  
    toKind(kind: number) {
      if (kind === this.kind) {
        return this;
      }
  
      const [wholePart, fractionalPart] = this.value.toString().split(".");
  
      if (!fractionalPart) {
        return new Number(parseInt(wholePart, this.kind).toString(kind), kind);
      }
  
      const zerosInBegin = (fractionalPart.match(/^0+/gi) || [""])[0];
  
      return new Number(
        parseInt(wholePart, this.kind).toString(kind) +
          "." +
          zerosInBegin +
          parseInt(fractionalPart, this.kind).toString(kind),
        kind
      );
    }
  }