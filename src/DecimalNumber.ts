import { Number } from "./Number"; './Number';

export class DecimalNumber extends Number {
    constructor(value: string | number){
      super(value, 10)
    }
  
    toBinaryCodedDecimal(): string {
      return this.value.toString().split('').map( (figure) => {
        const value = new Number(figure, 10).toKind(2).value.toString();
  
        return '0'.repeat(Math.max(0, 4-value.length)) + value
      } ).join(' ')
    }
  }