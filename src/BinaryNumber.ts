import { Number } from "./Number";

export class BinaryNumber extends Number {
  constructor(value: string | number | Number) {
    if (value instanceof Number) {
      super(value.toKind(2).value, 2);
      return;
    }

    super(value, 2);
  }

  static fromBinaryCodeDecimal(code: string | number, figureLength = 4) {
    const groups: string[] = [];

    code
      .toString()
      .replace(/\s/gi, "")
      .split("")
      .forEach((figure, index) => {
        const groupIndex = Math.ceil((index + 1) / figureLength) - 1;

        if (!groups[groupIndex]) {
          groups[groupIndex] = "";
        }

        groups[groupIndex] += figure;
      });
      

    return new BinaryNumber(
      new Number(
        groups
          .map((binaryNumber) => new Number(binaryNumber, 2).toKind(10).value)
          .join(""),
        10
      )
    );
  }
}
