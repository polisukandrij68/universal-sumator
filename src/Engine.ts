import { Number } from './Number';

export class Engine {
  private doOperation(
    number1: Number,
    number2: Number,
    operator: "+" | "-" | "*" | "/",
    resultKind?: number
  ): Number {
    const kind = resultKind || number1.kind;

    const [resultNumber1, resultNumber2] = [number1, number2].map((n) =>
      n.toKind(10)
    );

    return new Number(
      eval(
        `${parseFloat(resultNumber1.value.toString())} ${operator} ${parseFloat(
          resultNumber2.value.toString()
        )}`
      ),
      10
    ).toKind(kind);
  }

  sum(number1: Number, number2: Number, resultKind?: number): Number {
    return this.doOperation(number1, number2, "+", resultKind);
  }
  subtract(number1: Number, number2: Number, resultKind?: number): Number {
    return this.doOperation(number1, number2, "-", resultKind);
  }
  multiply(number1: Number, number2: Number, resultKind?: number): Number {
    return this.doOperation(number1, number2, "*", resultKind);
  }
}
